/* Sham data for unit testing */

export const sampleUser = {
  id: {
    value: 'A123',
  },
  email: 'johndoe@test.ac',
  name: {
    first: 'John',
    last: 'Doe',
  },
  login: {
    username: 'johndoe',
  },
  picture: {
    thumbnail: 'https://',
  },
};
