/* Utilities related to api calls */

/**
 * Function to fetch 50 random users from randomuser.me
 * and return them via a setter function.
 * Nationality param is also set here (if present).
 *
 * setterFunction {function}  The setter callback to store the fetched data.
 */
export const fetchUsers = (setterFunction) => {
  let url = 'https://randomuser.me/api/?results=50';

  // Check if nationality has been selected and add it to params
  const nationality = localStorage.getItem('catalogNationality');
  if (nationality) {
    url += `&nat=${nationality}`;
  }

  fetch(url)
    .then((res) => res.json())
    .then(({ results }) => setterFunction(results));
};
