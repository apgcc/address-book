/**
 * Main App node.
 * Navigation with routing is defined here.
 */

import React from 'react';
import './App.scss';
import './utils/styles.scss';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './scenes/Home';
import Settings from './scenes/Settings';

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/settings">
        <Settings />
      </Route>
    </Switch>
  </Router>
);

export default App;
