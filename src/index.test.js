import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

jest.mock('react-dom', () => ({ render: jest.fn() }));

test('renders with App and root', () => {
  const root = document.createElement('div');
  root.id = 'root';
  document.body.appendChild(root);

  // Requires index.js so that react-dom render method is called
  // eslint-disable-next-line global-require
  require('./index.js');

  expect(ReactDOM.render).toHaveBeenCalledWith((
    <React.StrictMode>
      <App />
    </React.StrictMode>
  ), root);
});
