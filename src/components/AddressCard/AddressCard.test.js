import React from 'react';
import { render } from '@testing-library/react';
import AddressCard from './index';
import { sampleUser } from '../../utils/tests';

test('renders correctly', () => {
  const { getByText, getByAltText } = render(<AddressCard user={sampleUser} />);

  getByText(`${sampleUser.name.first} ${sampleUser.name.last}`);
  getByText(sampleUser.login.username);
  getByText(sampleUser.email);
  getByAltText(`User ${sampleUser.login.username}`);
});
