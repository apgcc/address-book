/**
 * Single address card showing some details about the user.
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const AddressCard = ({ user }) => (
  <div
    id={`address-card-user-${user.id.value}`}
    className="address-card"
  >
    <img
      src={user.picture.thumbnail}
      alt={`User ${user.login.username}`}
    />

    <div className="address-card-info">
      <span className="address-card-name">
        {user.name.first}
        {' '}
        {user.name.last}
      </span>
      <span>
        {user.login.username}
      </span>
      <span>
        {user.email}
      </span>
    </div>
  </div>
);

AddressCard.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.shape({
      value: PropTypes.string.isRequired,
    }).isRequired,
    email: PropTypes.string.isRequired,
    picture: PropTypes.shape({
      thumbnail: PropTypes.string.isRequired,
    }),
    name: PropTypes.shape({
      first: PropTypes.string.isRequired,
      last: PropTypes.string.isRequired,
    }).isRequired,
    login: PropTypes.shape({
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default AddressCard;
