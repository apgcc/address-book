import React from 'react';
import { render } from '@testing-library/react';
import Catalog from './index';
import { sampleUser } from '../../utils/tests';

test('renders correctly with users', () => {
  const loadMore = jest.fn();

  const { getByText } = render(
    <Catalog
      users={[sampleUser]}
      inputValue=""
      loadMore={loadMore}
    />,
  );
  getByText('John Doe');
});

test('renders correctly with no users', () => {
  const loadMore = jest.fn();

  const { getByText } = render(
    <Catalog
      users={[sampleUser]}
      inputValue="lorem"
      loadMore={loadMore}
    />,
  );
  getByText('No matches found.');
});
