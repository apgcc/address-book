/**
 * Catalog of users + loader message at the bottom.
 * Implemented with InfiniteScroll library.
 */

import React from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import AddressCard from '../AddressCard';
import './style.scss';

const Catalog = ({ users, loadMore, inputValue }) => {
  const MAX_LIMIT = 1000;

  // Filter users by searched term
  const filteredUsers = users.filter((user) => {
    const name = `${user.name.first} ${user.name.last}`.toUpperCase();
    const value = inputValue.toUpperCase();
    return name.includes(value);
  });

  // If there are no people corresponding to search param, notify user with this message
  if (users.length && !filteredUsers.length) {
    return (
      <p className="message">
        <span>No matches found.</span>
      </p>
    );
  }

  return (
    <InfiniteScroll
      dataLength={users.length}
      next={loadMore}
      hasMore={!inputValue && users.length < MAX_LIMIT}
      scrollThreshold={0.7}
      loader={(
        /* Message shown when the fetch action is triggered again */
        <div id="loader-container">
          <div id="loader" />
          <span>Loading...</span>
        </div>
      )}
      endMessage={(inputValue
        ? (
          /* Don't fetch while searching */
          <p className="message">
            <span>Cannot fetch any further during search.</span>
          </p>
        ) : (
          /* Message shown after fetching 1000 people */
          <p className="message">
            <span>End of users catalog.</span>
          </p>
        )
      )}
    >
      {filteredUsers.map((user) => <AddressCard key={user.id.value} user={user} />)}
    </InfiniteScroll>
  );
};

Catalog.propTypes = {
  inputValue: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  loadMore: PropTypes.func.isRequired,
};

export default Catalog;
