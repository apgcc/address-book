import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { render } from '@testing-library/react';
import Settings from './index';

test('renders correctly', () => {
  const { getByText } = render(
    <Router>
      <Settings />
    </Router>,
  );

  getByText('Filter by nationality:');
});
