import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

const Settings = () => {
  const [selectValue, setSelectValue] = useState(localStorage.getItem('catalogNationality') ?? '');

  const handleChange = (value) => {
    setSelectValue(value);
    localStorage.setItem('catalogNationality', value);
  };

  return (
    <div className="container">
      <header>
        <div />
        <Link to="/">Home</Link>
      </header>
      <main>
        <label htmlFor="nationality">
          Filter by nationality:
          <select
            name="nationality"
            id="nationality"
            onChange={(e) => handleChange(e.target.value)}
            value={selectValue}
          >
            <option value="">-</option>
            <option value="ch">Switzerland</option>
            <option value="es">Spain</option>
            <option value="fr">France</option>
            <option value="gb">Great Britain</option>
          </select>
        </label>
      </main>
    </div>
  );
};

export default Settings;
