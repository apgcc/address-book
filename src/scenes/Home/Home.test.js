import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { fireEvent, render } from '@testing-library/react';
import Home from './index';

test('renders correctly', () => {
  const { getByText, getByPlaceholderText } = render(
    <Router>
      <Home />
    </Router>,
  );

  getByText('Settings');

  const input = getByPlaceholderText('Search by name...');
  fireEvent.change(input, { target: { value: 'test' } });
  expect(input.value).toBe('test');
});
