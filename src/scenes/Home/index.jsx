/**
 * Initial page containing the list of users in the address book (catalog).
 */

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import Catalog from '../../components/Catalog';
import { fetchUsers } from '../../utils/api';

const Home = () => {
  const [users, setUsers] = useState([]);
  const [inputValue, setInputValue] = useState('');

  // Initial fetch on mount
  useEffect(() => {
    fetchUsers(setUsers);
  }, []);

  return (
    <div className="container">
      <header>
        <input
          placeholder="Search by name..."
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <Link to="/settings">Settings</Link>
      </header>
      <main>
        <Catalog
          users={users}
          inputValue={inputValue}
          loadMore={() => fetchUsers((batch) => setUsers(users.concat(batch)))}
        />
      </main>
    </div>
  );
};

export default Home;
