# Address Book App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Author: Gerardo Aprili (apgsn@yahoo.com).

## Overview of the project

This app includes all the minimum expected requirements (users' catalog, infinite scroll, search bar, settings page), plus persisting storage of the nationality setting and pre-fetch of users. To run it, clone the repo, access the root folder, then:
```
npm install
npm start
```
To perform testing:
```
npm test
```
A note on testing: unfortunately I've never had the chance to do unit testing on React components before, so don't expect 100% coverage as I've encountered some issues in laying out the correct tests (namely due to not having enough time to properly learn the ropes of Jest).
